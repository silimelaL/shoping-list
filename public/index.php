<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\RouterConfig;
use App\Controller\shoppingListController;

$router = new RouterConfig();

$router->get('/', shoppingListController::class . '::index');
$router->get('/checkItem/{id}', shoppingListController::class . '::check');
$router->get('/deleteItem/{id}', shoppingListController::class . '::delete');
$router->post('/store', shoppingListController::class . '::store');
$router->post('/update/{id}', shoppingListController::class . '::update');

$router->run();
