# PHP-Minii

A lightweight PHP framework designed to make web development faster and easier, without sacrificing flexibility or performance

## Installation

- Clone this repository 
- edit `.env` file with your database credentials
- Install composer dependencies: `composer install`
- download database from this link `https://drive.google.com/file/d/1SU3aWQy4B8ULbl_q6X96tWUKLrxG0Ehq/view`
- Populate Database
- navigate to the `public` directory
- serve app: `php -S localHost:9000`

