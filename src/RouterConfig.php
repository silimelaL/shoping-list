<?php

namespace App;

class RouterConfig
{

    private array $handlers;
    private const METHOD_POST = 'POST';
    private const METHOD_GET = 'GET';


    public function get(string $path, $handler): void
    {
        $this->addHandler(self::METHOD_GET, $path, $handler);
    }

    public function post(string $path, $handler): void
    {
        $this->addHandler(self::METHOD_POST, $path, $handler);
    }

    private function addHandler(string $method, string $path, $handler): void
    {
        $this->handlers[$method . $path] = [
            'path' => $path,
            'method' => $method,
            'handler' => $handler,
        ];
    }

    public function run()
    {
        $requestUri = parse_url($_SERVER['REQUEST_URI']);
        $requestPath =  $requestUri['path'];
        $method  = $_SERVER['REQUEST_METHOD'];

        $callback = null;
        foreach ($this->handlers as $handler) {
            // Check if the handler path contains a variable, denoted by '{' and '}'
            if (strpos($handler['path'], '{') !== false && strpos($handler['path'], '}') !== false) {
                $regex = str_replace('/', '\/', $handler['path']);
                $regex = str_replace('{', '(?P<', $regex);
                $regex = str_replace('}', '>[^\/]+)', $regex);
                preg_match('/^' . $regex . '$/', $requestPath, $matches);
                if ($matches && $method === $handler['method']) {
                    $callback = $handler['handler'];
                    array_shift($matches);
                    $callbackParams = $matches;
                    break;
                }
            } elseif ($handler['path'] == $requestPath && $method === $handler['method']) {
                $callback = $handler['handler'];
                $callbackParams = [];
                break;
            }
        }

        if (is_string($callback)) {
            $parts = explode('::', $callback);
            if (is_array($parts)) {
                $className = array_shift($parts);
                $handler = new $className;

                $method = array_shift($parts);
                $callback = [$handler, $method];
            }
        }

        if (!$callback) {
            header("HTTP/1.0 404 Not Found");
            return;
        }

        $params = array_merge($_GET, $_POST);
        if (isset($callbackParams)) {
            $params = array_merge($params, $callbackParams);
        }

        call_user_func_array($callback, [$params]);
    }
}
