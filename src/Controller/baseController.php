<?php

namespace App\Controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class baseController
{

    public function response($pageName, $data)
    {

        $loader = new FilesystemLoader(__DIR__ . '/../../templates');
        $twig = new Environment($loader, [
            'cache' => __DIR__ . '/cache',
            'auto_reload' => true
        ]);

        $template = $twig->load($pageName . '.html.twig');
        $content = $template->render(
            $data
        );

        echo  $content;
    }
}
