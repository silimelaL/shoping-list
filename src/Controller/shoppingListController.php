<?php

namespace App\Controller;

use App\Controller\baseController;
use App\Database\DatabaseConnection;

class shoppingListController extends baseController
{

    public function index()
    {
        $db = new DatabaseConnection();

        $pdo = $db->getPdo();
        $stmt = $pdo->query('SELECT * FROM items');
        $results = $stmt->fetchAll();
        return $this->response(
            'index',
            ['items' => $results]
        );
    }

    public function update($params)
    {

        $id = $params['id'];
        $item_name = $params['item_name'];

        if (!empty($item_name)) {
            $db = new DatabaseConnection();
            $pdo = $db->getPdo();
            $stmt = $pdo->prepare("UPDATE items SET name=:name WHERE id=:id");
            $stmt->bindParam(":id", $id);
            $stmt->bindParam(":name", $item_name);

            $stmt->execute();
        }

        header("Location: /");
        exit;
    }

    public function store($params)
    {
        $itemName = $params['item_name'];

        if ($itemName) {
            $db = new DatabaseConnection();
            $pdo = $db->getPdo();
            $stmt = $pdo->prepare("INSERT INTO items (name) VALUES (:name)");
            $stmt->execute(['name' => $itemName]);
        }
        header("Location: /");
        exit;
    }

    public function check($params)
    {

        $id = $params['id'];
        $db = new DatabaseConnection();
        $pdo = $db->getPdo();
        $stmt = $pdo->prepare("UPDATE items SET checked=1 WHERE id=:id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();

        header("Location: /");
        exit;
    }

    public function delete($params)
    {
        $id = $params['id'];
        $db = new DatabaseConnection();
        $pdo = $db->getPdo();
        $query = "DELETE FROM items WHERE id = :id";
        $statement = $pdo->prepare($query);
        $statement->bindValue(':id', $id);
        $statement->execute();

        // Redirect back to the list page
        header("Location: /");
        exit;
    }
}
