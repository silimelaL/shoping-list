<?php

namespace App\Database;

use PDO;
use PDOException;

class DatabaseConnection
{
    private $pdo;

    public function __construct()
    {
        $config = include 'Config.php';

        $dsn = "mysql:host={$config['database']['host']};dbname={$config['database']['dbname']}";
        $username = $config['database']['username'];
        $password = $config['database']['password'];

        try {
            $this->pdo = new PDO($dsn, $username, $password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function getPdo()
    {
        return $this->pdo;
    }
}
